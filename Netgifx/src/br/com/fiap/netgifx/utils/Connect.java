package br.com.fiap.netgifx.utils;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import br.com.fiap.netgifx.entities.UserEntity;
   
  public class Connect {
       
	  Connection con = null;
   
       public Connect() throws SQLException {
   
            try {
                  Class.forName("com.mysql.jdbc.Driver");

            } catch (ClassNotFoundException e) {
                  e.printStackTrace();
            }
   
            String url = "jdbc:mysql://localhost:3306/netgif";
            String user = "root";
            String password = "fiap";
            con = DriverManager.getConnection(url, user, password);
            System.out.println("Conex�o estabelecida!");
       }
   
       public void closeConnection() throws SQLException {
            con.close();
            System.out.println("Conex�o fechada!");
       }
   
       public boolean insertUsuario(UserEntity usuario) throws SQLException {
   
            Statement st = null;
            //ResultSet rs = null;
  
            try {
            	
		          if (usuario.getName().isEmpty()) {
		        	  addMessage("Preencha o nome completo!");
		        	  return false;			
		        	 }
		            	
		          if (usuario.getLogin().isEmpty()) {
		        	  addMessage("Preencha o usu�rio!");
		    		  return false;			
		    		 }
		    		
		          if (usuario.getPassword().isEmpty()) {
		        	  addMessage("Preencha a senha!");
		    		  return false;			
		    	     }         	
            	     	
                  st = con.createStatement();
   
                  PreparedStatement preparedStatement = con.prepareStatement("insert into usuario (nome, login, senha, descricao, data_cadastro) values(?,?,?,?,?)");
                  preparedStatement.setString(1, usuario.getName());
                  preparedStatement.setString(2, usuario.getLogin());
                  preparedStatement.setString(3, usuario.getPassword());
                  preparedStatement.setString(4, usuario.getDescription());
                  preparedStatement.setDate(5, new java.sql.Date(new Date().getTime()));
   
                  preparedStatement.execute();
                  closeConnection();
                  return true;
            } catch (SQLException ex) {
                  Logger lgr = Logger.getLogger(Connect.class.getName());
                  lgr.log(Level.SEVERE, ex.getMessage(), ex);
                  closeConnection();
                  return false;
            }
       }
       
       public void addMessage(String summary) {
           FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary,  null);
           FacesContext.getCurrentInstance().addMessage(null, message);
       }       
       
  }