package br.com.fiap.netgifx.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

public class GifToPngConverter {

	public InputStream convert(InputStream is) {
		
		File output = new File("output.png");
		try {
			ImageIO.write(ImageIO.read(is), "png", output);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		InputStream rendered = null;
		
		try {
			rendered = new FileInputStream(output);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return rendered;
      
	}

		

}
