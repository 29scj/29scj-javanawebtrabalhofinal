package br.com.fiap.netgifx.services;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

import br.com.fiap.netgifx.dao.CategoryDAO;
import br.com.fiap.netgifx.entities.CategoryEntity;
import br.com.fiap.netgifx.models.Category;

@ManagedBean(name = "categoryService", eager = true)
@ApplicationScoped
public class CategoryService {

	@EJB
	private CategoryDAO categoryDAO;

	private List<Category> categories;

	public List<Category> findAll() {
		this.categories = new ArrayList<>();
		List<CategoryEntity> categoriesEntity = categoryDAO.findAll();
		for (CategoryEntity categoryEntity : categoriesEntity) {
			categories.add(categoryEntity.convert(true));
		}
		return categories;
	}

	public List<CategoryEntity> getCategories() {
		return categoryDAO.findAll();
	}

	public void save(Category category) {
		CategoryEntity categoryEntity = new CategoryEntity(category);
		this.categoryDAO.create(categoryEntity);
	}
}