package br.com.fiap.netgifx.services;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

import br.com.fiap.netgifx.dao.UserDAO;
import br.com.fiap.netgifx.entities.UserEntity;
import br.com.fiap.netgifx.models.User;

@ManagedBean(name = "userService", eager = true)
@ApplicationScoped
public class UserService {

	@EJB
	private UserDAO userDAO;

	private List<User> users;

	public List<User> findAll() {
		this.users = new ArrayList<>();
		List<UserEntity> usersEntity = userDAO.findAll();
		for (UserEntity userEntity : usersEntity) {
			users.add(userEntity.convert());
		}
		return users;
	}

	public void save(User user) {
		UserEntity userEntity = new UserEntity(user);
		this.userDAO.create(userEntity);
	}
}