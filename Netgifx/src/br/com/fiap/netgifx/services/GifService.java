package br.com.fiap.netgifx.services;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

import br.com.fiap.netgifx.dao.CategoryDAO;
import br.com.fiap.netgifx.dao.GifDAO;
import br.com.fiap.netgifx.dao.UserDAO;
import br.com.fiap.netgifx.entities.CategoryEntity;
import br.com.fiap.netgifx.entities.GifEntity;
import br.com.fiap.netgifx.entities.UserEntity;
import br.com.fiap.netgifx.models.Gif;

@ManagedBean(name = "gifService", eager = true)
@ApplicationScoped
public class GifService {

	@EJB
	private GifDAO gifDAO;

	@EJB
	private CategoryDAO categoryDAO;

	@EJB
	private UserDAO userDAO;

	private List<Gif> gifs;

	public List<Gif> findAll() {
		this.gifs = new ArrayList<>();
		List<GifEntity> gifsEntity = gifDAO.findAll();
		for (GifEntity gifEntity : gifsEntity) {
			this.gifs.add(gifEntity.convert(true));
		}
		return gifs;
	}

	public List<Gif> findByUserId(Long id) {
		return this.gifDAO.findByUserId(id);
	}

	public void save(Gif gif) {
		GifEntity gifEntity = new GifEntity(gif);
		CategoryEntity categoryEntity = this.categoryDAO.find(gif.getCategory().getId());
		gifEntity.setCategory(categoryEntity);
		this.gifDAO.save(gifEntity);
	}

	public void addFavorite(Long gifId, Long userId) {
		UserEntity userEntity = this.userDAO.find(userId);
		GifEntity gifEntity = this.gifDAO.find(gifId);
		userEntity.getGifsEntity().add(gifEntity);
		this.userDAO.save(userEntity);
	}
}