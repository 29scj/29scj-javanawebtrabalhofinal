package br.com.fiap.netgifx.fixture;

import javax.ejb.Stateless;
import javax.inject.Inject;

import br.com.fiap.netgifx.dao.UserDAO;
import br.com.fiap.netgifx.entities.UserEntity;

@Stateless
public class CreateUsers {

	@Inject
	UserDAO userDAO;

	public void create() {

		System.out.println("FIXTURES: CREATING USERS");

		UserEntity entity = new UserEntity();
		entity.setLogin("admin");
		entity.setPassword("admin");
		entity.setName("Administrador");
		entity.setDescription("Administrador do sistema");
		entity.setAdmin(true);
		userDAO.create(entity);

		entity = new UserEntity();
		entity.setLogin("user");
		entity.setPassword("user");
		entity.setName("Comum");
		entity.setDescription("Uusario comum do sistema");
		entity.setAdmin(false);
		userDAO.create(entity);
	}
}
