package br.com.fiap.netgifx.fixture;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

@Singleton
@Startup
public class Initialize {

	@Inject
	CreateUsers createUsers;
	
	@Inject
	CreateCategories createCategories;
	
	@Inject
	ImportDemoGifs demoGifs;
	
	
	@PostConstruct
	public void init() {
		
		System.out.println("Creating admin");
		createUsers.create();
		
		System.out.println("Creating categories");
		createCategories.create();
	
		//System.out.println("Importing Gifs");
		//demoGifs.create();
		;
	}

}
