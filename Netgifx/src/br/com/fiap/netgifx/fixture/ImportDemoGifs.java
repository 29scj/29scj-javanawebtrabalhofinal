package br.com.fiap.netgifx.fixture;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.List;
import java.util.Random;

import javax.ejb.Stateless;
import javax.inject.Inject;

import br.com.fiap.netgifx.dao.CategoryDAO;
import br.com.fiap.netgifx.dao.GifDAO;
import br.com.fiap.netgifx.entities.CategoryEntity;
import br.com.fiap.netgifx.entities.GifEntity;
import br.com.fiap.netgifx.models.Genre;
import br.com.fiap.netgifx.models.Language;
import br.com.fiap.netgifx.models.Rating;
import br.com.fiap.netgifx.utils.GifToPngConverter;
import br.com.fiap.netgifx.utils.InputStreamToBytesConverter;

@Stateless
public class ImportDemoGifs {

	@Inject
	GifDAO gifDao;

	@Inject
	CategoryDAO categoryDao;

	// Gif Names
	private String[] gifNames = { "1", "2", "3", "4" };

	// Gif Titles
	private String[] gifTitles = { "Aviao", "Rato", "Camelo", "Laranja", "Marshmellow" };

	private List<CategoryEntity> catList;

	public void create() {

		this.catList = categoryDao.findAll();
		
		byte[] gifArray;
		byte[] gifSnapshotArray;
		InputStream imgSnapshot = null;

		// Upload Example Gifs on bunch
		for (int i = 0; i < 25; i++) {
			System.out.println("Iteration number #" + i);
			GifEntity gif = new GifEntity();
			String randomGif = this.randomGif();
			System.out.println("Gif name: " + randomGif + ".gif");
			File file = new File(getClass().getClassLoader()
					.getResource("br/com/fiap/netgifx/fixture/gifs/" + randomGif + ".gif").getPath());
			
			FileInputStream fis;
			try {
				fis = new FileInputStream(file);
				imgSnapshot = new GifToPngConverter().convert(fis);
				
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			try {

				// Create objects
				gifArray = Files.readAllBytes(file.toPath());
				gifSnapshotArray = new InputStreamToBytesConverter().readBytes(imgSnapshot);
				gif.setFilename(file.getName());
				gif.setImage(gifArray);
				gif.setImageSnapshot(gifSnapshotArray);
				gif.setName(this.randomTitle());
				gif.setGenre(this.randomGenre());
				gif.setLanguage(this.randomLanguage());
				gif.setRating(this.randomRating());
				gif.setCategory(this.randomCategory());

				// gif.setCategory(categoryDao.find(this.randomCategory().getId()));
				gifDao.create(gif);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	/**
	 * Randomize Gifs
	 * 
	 * @return
	 */
	public String randomGif() {
		return this.gifNames[new Random().nextInt(gifNames.length)];
	}

	/**
	 * Randmomize Titles
	 * 
	 * @return
	 */
	public String randomTitle() {
		return this.gifTitles[new Random().nextInt(this.gifTitles.length)];
	}

	/**
	 * Randomize categories
	 * 
	 * @return
	 */
	public CategoryEntity randomCategory() {
		Random random = new Random();
		int listSize = this.catList.size();
		int randomIndex = random.nextInt(listSize);
		return this.catList.get(randomIndex);
	}

	public Genre randomGenre() {
		Random random = new Random();
		int listSize = Genre.values().length;
		int randomIndex = random.nextInt(listSize);
		return Genre.values()[randomIndex];
	}

	public Language randomLanguage() {
		Random random = new Random();
		int listSize = Language.values().length;
		int randomIndex = random.nextInt(listSize);
		return Language.values()[randomIndex];
	}

	public Rating randomRating() {
		Random random = new Random();
		int listSize = Rating.values().length;
		int randomIndex = random.nextInt(listSize);
		return Rating.values()[randomIndex];
	}

}
