package br.com.fiap.netgifx.fixture;

import java.util.ArrayList;

import javax.ejb.Stateless;
import javax.inject.Inject;

import br.com.fiap.netgifx.dao.CategoryDAO;
import br.com.fiap.netgifx.entities.CategoryEntity;

@Stateless
public class CreateCategories {

	@Inject
	CategoryDAO categoryDAO;

	public void create() {

		System.out.println("FIXTURES: CREATING CATEGORIES SAMPLE");

		ArrayList<String> list = new ArrayList<String>();
		list.add("ESTREIA");
		list.add("CANCELADO");
		list.add("PREMIADO");

		for (String string : list) {

			CategoryEntity entity = new CategoryEntity();
			entity.setName(string);
			categoryDAO.create(entity);
		}
	}
}
