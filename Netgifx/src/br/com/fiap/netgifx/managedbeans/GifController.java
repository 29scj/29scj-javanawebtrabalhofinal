package br.com.fiap.netgifx.managedbeans;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;

import org.primefaces.model.UploadedFile;

import br.com.fiap.netgifx.entities.CategoryEntity;
import br.com.fiap.netgifx.models.Genre;
import br.com.fiap.netgifx.models.Gif;
import br.com.fiap.netgifx.models.Language;
import br.com.fiap.netgifx.models.Rating;
import br.com.fiap.netgifx.services.CategoryService;
import br.com.fiap.netgifx.services.GifService;
import br.com.fiap.netgifx.utils.GifToPngConverter;
import br.com.fiap.netgifx.utils.InputStreamToBytesConverter;

@RequestScoped
@ManagedBean
public class GifController implements Serializable {
	private static final long serialVersionUID = 8740152239472998952L;

	private Gif gif;
	private String name;
	private List<Gif> gifList;
	private CategoryEntity categoryEntity;
	private List<CategoryEntity> categoriesEntity;
	private Genre genre;
	private Language language;
	private Rating rating;
	private UploadedFile uploadedFile;
	private byte[] snap = null;
	private byte[] image = null;

	@ManagedProperty("#{categoryService}")
	private CategoryService categoryService;

	@ManagedProperty("#{gifService}")
	private GifService gifService;

	@PostConstruct
	public void init() {
		this.categoriesEntity = this.categoryService.getCategories();
		clear();
		this.findAll();
	}

	private void clear() {
		this.gifList = new ArrayList<>();
		this.genre = null;
		this.language = null;
		this.rating = null;
		this.uploadedFile = null;
	}

	private void findAll() {
		this.gifList = this.gifService.findAll();
	}

	public void upload() {
		try {
			InputStream input = uploadedFile.getInputstream();
			this.snap = new InputStreamToBytesConverter().readBytes(new GifToPngConverter().convert(input));
			this.image = new InputStreamToBytesConverter().readBytes(input);
		} catch (IOException e) {
			e.printStackTrace();
		}
		this.addGif();
		this.findAll();
	}

	public void addGif() {
		Gif gif = new Gif();
		gif.setName(this.name);
		gif.setImageSnapshot(this.snap);
		gif.setFilename(this.uploadedFile.getFileName());
		gif.setImage(this.image);
		gif.setCategory(this.categoryEntity.convert(true));
		gif.setGenre(this.genre);
		gif.setLanguage(this.language);
		gif.setRating(this.rating);
		this.gifService.save(gif);
		this.findAll();
		this.clear();
	}

	public Gif getGif() {
		return gif;
	}

	public void setGif(Gif gif) {
		this.gif = gif;
	}

	public void setCategoryService(CategoryService service) {
		this.categoryService = service;
	}

	public List<CategoryEntity> getCategoriesEntity() {
		return categoriesEntity;
	}

	public void setCategoriesEntity(List<CategoryEntity> categoriesEntity) {
		this.categoriesEntity = categoriesEntity;
	}

	public CategoryEntity getCategoryEntity() {
		return categoryEntity;
	}

	public void setCategoryEntity(CategoryEntity categoryEntity) {
		this.categoryEntity = categoryEntity;
	}

	public void setGifService(GifService gifService) {
		this.gifService = gifService;
	}

	public List<Gif> getGifList() {
		return gifList;
	}

	public UploadedFile getUploadedFile() {
		return uploadedFile;
	}

	public void setUploadedFile(UploadedFile uploadedFile) {
		this.uploadedFile = uploadedFile;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Genre[] getGenres() {
		return Genre.values();
	}

	public Genre getGenre() {
		return genre;
	}

	public void setGenre(Genre genre) {
		this.genre = genre;
	}

	public Language getLanguage() {
		return language;
	}

	public void setLanguage(Language language) {
		this.language = language;
	}

	public Language[] getLanguages() {
		return Language.values();
	}

	public Rating getRating() {
		return rating;
	}

	public void setRating(Rating rating) {
		this.rating = rating;
	}

	public Rating[] getRatings() {
		return Rating.values();
	}

	public byte[] getSnap() {
		return snap;
	}

	public void setSnap(byte[] snap) {
		this.snap = snap;
	}

	public byte[] getImage() {
		return image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public CategoryService getCategoryService() {
		return categoryService;
	}

	public GifService getGifService() {
		return gifService;
	}

	public void setGifList(List<Gif> gifList) {
		this.gifList = gifList;
	}
	
}
