package br.com.fiap.netgifx.managedbeans.converter;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

import br.com.fiap.netgifx.entities.CategoryEntity;
import br.com.fiap.netgifx.services.CategoryService;
 
@FacesConverter("categoryConverter")
public class CategoryConverter implements Converter {
		 
    public Object getAsObject(FacesContext fc, UIComponent uic, String value) {
        if(value != null && value.trim().length() > 0) {
            try {
            	CategoryService service = (CategoryService) fc.getExternalContext().getApplicationMap().get("categoryService");
                return service.getCategories().get(Integer.parseInt(value)-1);
            } catch(NumberFormatException e) {
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error", "Not a valid theme."));
            }
        }
        else {
            return null;
        }
    }
 
    public String getAsString(FacesContext fc, UIComponent uic, Object object) {
        if(object != null) {
            return String.valueOf(((CategoryEntity) object).getId());
        }
        else {
            return null;
        }
    }   
}         