package br.com.fiap.netgifx.managedbeans;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;

import br.com.fiap.netgifx.models.Gif;
import br.com.fiap.netgifx.models.User;
import br.com.fiap.netgifx.services.UserService;

@RequestScoped
@ManagedBean
public class UserController {

	private User user;

	private List<User> userList;

	private List<Gif> favorites;

	@ManagedProperty("#{userService}")
	private UserService userService;

	@PostConstruct
	public void init() {
		this.user = new User();
		this.findAll();
	}

	private void findAll() {
		this.userList = this.userService.findAll();
	}

	public void addUser() {
		this.userService.save(this.user);
		findAll();
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public void setUserList(List<User> userList) {
		this.userList = userList;
	}

	public List<User> getUserList() {
		return this.userList;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public List<Gif> getFavorites() {
		return favorites;
	}

	public void setFavorites(List<Gif> favorites) {
		this.favorites = favorites;
	}
	
}