package br.com.fiap.netgifx.managedbeans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import br.com.fiap.netgifx.dao.CategoryDAO;
import br.com.fiap.netgifx.entities.CategoryEntity;
import br.com.fiap.netgifx.models.Category;
import br.com.fiap.netgifx.models.Gif;
import br.com.fiap.netgifx.models.User;
import br.com.fiap.netgifx.services.GifService;

@ManagedBean
@ViewScoped
public class HomeController implements Serializable {
	private static final long serialVersionUID = 5167875040505147009L;

	@EJB
	private CategoryDAO categoryDAO;

	private List<Category> categories;
	private List<Gif> gifExamples;

	private Gif selectedGif;

	private List<Gif> favorites;
	
	@ManagedProperty("#{gifService}")
	private GifService gifService;

	@PostConstruct
	public void init() {
		this.getCategories();
	}

	public List<Category> getCategories() {
		List<CategoryEntity> categoriesEntity = categoryDAO.findAll();
		this.categories = new ArrayList<>();
		this.gifExamples = new ArrayList<>();
		Category category;
		for (CategoryEntity categoryEntity : categoriesEntity) {
			category = categoryEntity.convert(true);
			this.categories.add(category);
			this.gifExamples.addAll(category.getGifs());
		}
		return this.categories;
	}

	public String addFavorite() {
		User currentUser = (User) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("user");
		this.gifService.addFavorite(this.selectedGif.getId(), currentUser.getId());
		return "favorite";
	}

	public List<Gif> getGifExamples() {
		return gifExamples;
	}

	public Gif getSelectedGif() {
		return selectedGif;
	}

	public void setSelectedGif(Gif selectedGif) {
		this.selectedGif = selectedGif;
	}

	public List<Gif> getFavorites() {
		return favorites;
	}

	public void setFavorites(List<Gif> favorites) {
		this.favorites = favorites;
	}

	public void setGifService(GifService gifService) {
		this.gifService = gifService;
	}
}
