package br.com.fiap.netgifx.managedbeans;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;

import br.com.fiap.netgifx.models.Category;
import br.com.fiap.netgifx.services.CategoryService;

@RequestScoped
@ManagedBean
public class CategoryController implements Serializable {
	private static final long serialVersionUID = -3247543052758218644L;

	private Category category;

	private List<Category> categoryList;

	@ManagedProperty("#{categoryService}")
	private CategoryService categoryService;

	@PostConstruct
	public void init() {
		this.category = new Category();
		this.fildAll();
	}

	private void fildAll() {
		this.categoryList = this.categoryService.findAll();
	}

	public void addCategory() {
		this.categoryService.save(this.category);
		fildAll();
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public void setCategoryList(List<Category> categoryList) {
		this.categoryList = categoryList;
	}

	public List<Category> getCategoryList() {
		return this.categoryList;
	}

	public void setCategoryService(CategoryService categoryService) {
		this.categoryService = categoryService;
	}
	
}
