package br.com.fiap.netgifx.managedbeans;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import br.com.fiap.netgifx.models.Gif;
import br.com.fiap.netgifx.models.User;
import br.com.fiap.netgifx.services.GifService;

@ViewScoped
@ManagedBean
public class FavoriteController {

	private List<Gif> favorites;
	private User currentUser;

	private Gif selectedGif;
	
	@ManagedProperty("#{gifService}")
	private GifService gifService;

	@PostConstruct
	public void init() {
		this.currentUser = (User) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("user");
		this.favorites = this.gifService.findByUserId(currentUser.getId());
	}

	public List<Gif> getFavorites() {
		return favorites;
	}

	public void setFavorites(List<Gif> favorites) {
		this.favorites = favorites;
	}

	public Gif getSelectedGif() {
		return selectedGif;
	}

	public void setSelectedGif(Gif selectedGif) {
		this.selectedGif = selectedGif;
	}

	public void setGifService(GifService gifService) {
		this.gifService = gifService;
	}
}