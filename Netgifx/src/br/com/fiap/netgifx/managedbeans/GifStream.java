package br.com.fiap.netgifx.managedbeans;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import javax.ejb.EJB;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseId;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import br.com.fiap.netgifx.dao.GifDAO;
import br.com.fiap.netgifx.entities.GifEntity;

@ApplicationScoped
@ManagedBean
public class GifStream {
	
	@EJB
	private GifDAO gifDAO;
	
	public StreamedContent getImage() throws IOException {
        FacesContext context = FacesContext.getCurrentInstance();

        if (context.getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
        	// Retorna um sub do StremedContent, que ira gerar a URL correta
            return new DefaultStreamedContent();
        }
        else {
            // Navegador solicita a imagem e retorna um StreamContent com os bytes da imagem.
            String gifId = context.getExternalContext().getRequestParameterMap().get("gifId");
            GifEntity gif = gifDAO.find(Long.valueOf(gifId));
            return new DefaultStreamedContent(new ByteArrayInputStream(gif.getImage()));
        }
    }
	
	public StreamedContent getSnapshot() throws IOException {
        FacesContext context = FacesContext.getCurrentInstance();

        if (context.getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
        	// Retorna um sub do StremedContent, que ira gerar a URL correta
            return new DefaultStreamedContent();
        }
        else {
            // Navegador solicita a imagem e retorna um StreamContent com os bytes da imagem.
            String gifId = context.getExternalContext().getRequestParameterMap().get("gifId");
            GifEntity gif = gifDAO.find(Long.valueOf(gifId));
            return new DefaultStreamedContent(new ByteArrayInputStream(gif.getImageSnapshot()));
        }
    }

}
