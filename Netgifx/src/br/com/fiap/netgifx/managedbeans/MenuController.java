package br.com.fiap.netgifx.managedbeans;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.DefaultSeparator;
import org.primefaces.model.menu.DefaultSubMenu;
import org.primefaces.model.menu.MenuModel;

import br.com.fiap.netgifx.models.User;

@ViewScoped
@ManagedBean
public class MenuController {

	private MenuModel model;
	private User currentUser;
	
	@PostConstruct
    public void init() {
		model = new DefaultMenuModel();
		model.addElement(menuHome());

		this.currentUser = (User) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("user");

		if (currentUser != null) {
			model.addElement(menuFavorite());

			if (currentUser.isAdmin()) {
				model.addElement(this.createCategory());
				model.addElement(this.createGif());
				model.addElement(this.createUser());
			}
			model.addElement(menuProfile(currentUser));
		}
    }

	private DefaultMenuItem menuHome() {
		DefaultMenuItem home = new DefaultMenuItem("P�gina Inicial");
		home.setUrl("/home.xhtml");
		return home;
	}

	private DefaultMenuItem menuFavorite() {
		DefaultMenuItem menuFavorite = new DefaultMenuItem("Favoritos");
		menuFavorite.setUrl("/favorite.xhtml");
		return menuFavorite;
	}

	private DefaultSubMenu menuProfile(User user) {
		DefaultSubMenu menuUser = new DefaultSubMenu(user.getName());

		DefaultMenuItem profile = new DefaultMenuItem("Perfil");
		profile.setUrl("/user/profile.xhtml");
		menuUser.addElement(profile);
		menuUser.addElement(new DefaultSeparator());

		DefaultMenuItem logout = new DefaultMenuItem("Logout");
		logout.setCommand("#{loginController.logout()}");
		//logout.setUrl("/login.xhtml");
		menuUser.addElement(logout);

		return menuUser;
	}

	private DefaultMenuItem createCategory() {
		DefaultMenuItem createCategory = new DefaultMenuItem("Categorias");
		createCategory.setUrl("/category/create-category.xhtml");
		return createCategory;
	}

	private DefaultMenuItem createGif() {
		DefaultMenuItem createGif = new DefaultMenuItem("Gifs");
		createGif.setUrl("/gif/create-gif.xhtml");
		return createGif;
	}

	private DefaultMenuItem createUser() {
		DefaultMenuItem createUser = new DefaultMenuItem("Usu�rios");
		createUser.setUrl("/user/create-user.xhtml");
		return createUser;
	}

	public MenuModel getModel() {
		return model;
	}

	public User getCurrentUser() {
		return currentUser;
	}
}