package br.com.fiap.netgifx.managedbeans;
import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import br.com.fiap.netgifx.entities.GifEntity;
 
@ManagedBean
@ViewScoped
public class GifDetailController implements Serializable {
	private static final long serialVersionUID = -5426333212123264803L;

	private GifEntity selectedGif = new GifEntity();

	public GifEntity getSelectedGif() {
		return selectedGif;
	}

	public void setSelectedGif(GifEntity selectedGif) {
		this.selectedGif = selectedGif;
	}
     
}