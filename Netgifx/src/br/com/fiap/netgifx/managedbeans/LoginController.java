package br.com.fiap.netgifx.managedbeans;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import br.com.fiap.netgifx.dao.UserDAO;
import br.com.fiap.netgifx.models.User;

@SessionScoped
@ManagedBean
public class LoginController {

	@EJB
	private UserDAO userDAO;
	private String login;
	private String password;

	public String send() {
		this.invalidateSession();
		if (!login.isEmpty() && !password.isEmpty()) {
			User user = new User();
			user = this.userDAO.findByLoginAndPassword(login, password);

			if (user == null) {
				addMessage("Usuario nao encontrado!");
				return null;

			} else {
				FacesContext context = FacesContext.getCurrentInstance();
				context.getExternalContext().getSessionMap().put("user", user);
				return "/home";
			}
		} else {
			addMessage("Favor prencher os campos Usuario e Senha!");
			return null;
		}
	}

	public String logout() {
		this.invalidateSession();
		return "/login.xhtml";
	}

	public void invalidateSession() {
		FacesContext context = FacesContext.getCurrentInstance();
		context.getExternalContext().getSessionMap().put("user", null);
	}

	public void addMessage(String summary) {
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, null);
		FacesContext.getCurrentInstance().addMessage(null, message);
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String login() {
		return "/login";
	}
}