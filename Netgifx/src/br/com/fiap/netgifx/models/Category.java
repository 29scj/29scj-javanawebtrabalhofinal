package br.com.fiap.netgifx.models;

import java.util.List;

public class Category {

	private Long id;
	private String name;
	private List<Gif> gifs;

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<Gif> getGifs() {
		return gifs;
	}
	public void setGifs(List<Gif> gifs) {
		this.gifs = gifs;
	}
	
	
}
