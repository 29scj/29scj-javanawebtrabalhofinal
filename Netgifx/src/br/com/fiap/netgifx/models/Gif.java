package br.com.fiap.netgifx.models;

public class Gif {

	private Long id;
	private String name;
	private String filename;
	private byte[] image;
	private Genre genre;
	private Language language;
	private Rating rating;
	private byte[] imageSnapshot;
	private Category category;
	
	public Gif() {
		super();
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public byte[] getImage() {
		return image;
	}
	public void setImage(byte[] image) {
		this.image = image;
	}
	public Genre getGenre() {
		return genre;
	}
	public void setGenre(Genre genre) {
		this.genre = genre;
	}
	public Language getLanguage() {
		return language;
	}
	public void setLanguage(Language language) {
		this.language = language;
	}
	public Rating getRating() {
		return rating;
	}
	public void setRating(Rating rating) {
		this.rating = rating;
	}
	public byte[] getImageSnapshot() {
		return imageSnapshot;
	}
	public void setImageSnapshot(byte[] imageSnapshot) {
		this.imageSnapshot = imageSnapshot;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}
	
}

