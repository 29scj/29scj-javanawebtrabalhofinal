package br.com.fiap.netgifx.models;

public enum Language {
	
	PORTUGUESE("PORTUGUES"),
	ENGLISH("INGLES"),
	SPANISH("ESPANHOL");
	
	private final String text;

	private Language(String text) {
		this.text = text;
	}
	
	public String getText() {
		return text;
	}
	

}
