package br.com.fiap.netgifx.models;

public enum Rating {
	
	UNRATED,
    G,
    PG,
    PG13,
    R,
    NC17

}
