package br.com.fiap.netgifx.models;

public enum Genre {

	ADVENTURE("AVENTURA"),
	COMEDY("COMEDIA"), 
	SCIENCE_FICTION("FICCAO CIENTIFICA"), 
	WAR("GUERRA"), 
	HISTORY("HISTORICOS"), 
	POLITICS("POLITICOS"), 
	LGBT("TEMATICA LGBT"), 
	DRAMA("DRAMATICOS"), 
	MAGIC("MAGIA");
	
	private final String text;

	private Genre(String text) {
		this.text = text;
	}
	
	public String getText() {
		return text;
	}

}
