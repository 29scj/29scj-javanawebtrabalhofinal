package br.com.fiap.netgifx.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import br.com.fiap.netgifx.models.Category;
import br.com.fiap.netgifx.models.Gif;

@Entity
@Table(name = "CATEGORY")
public class CategoryEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column
	private String name;

	@OneToMany(mappedBy = "category", fetch = FetchType.EAGER)
	private List<GifEntity> gifEntity;

	public CategoryEntity() {
		super();
	}

	public CategoryEntity(Category category) {
		this.setName(category.getName());
	}
	
	public Category convert(boolean isRoot) {
		Category category = new Category();
		category.setId(this.getId());
		category.setName(this.getName());
		if (isRoot) {
			if (this.getGifEntity() != null) {
				List<Gif> gifs = new ArrayList<>();
				Gif gif;
				for (GifEntity gifAux : gifEntity) {
					gif = gifAux.convert(false);
					gifs.add(gif);
				}
				category.setGifs(gifs);
			}
		}
		return category;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return name;
	}

	public List<GifEntity> getGifEntity() {
		return gifEntity;
	}

	public void setGifEntity(List<GifEntity> gifEntity) {
		this.gifEntity = gifEntity;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CategoryEntity other = (CategoryEntity) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
