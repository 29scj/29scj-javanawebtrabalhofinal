package br.com.fiap.netgifx.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.fiap.netgifx.models.Genre;
import br.com.fiap.netgifx.models.Gif;
import br.com.fiap.netgifx.models.Language;
import br.com.fiap.netgifx.models.Rating;

@Entity
@Table(name = "GIF")
public class GifEntity implements Serializable {
	private static final long serialVersionUID = 1413465359016214466L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column
	private String name;
	@Column
	private String filename;

	@ManyToOne(cascade={CascadeType.ALL})
	@JoinColumn(name = "categoryId")
	private CategoryEntity category;

	@ManyToMany(fetch = FetchType.EAGER, mappedBy = "gifsEntity")
	private List<UserEntity> users = new ArrayList<>();

	@Column
	@Lob
	private byte[] image;

	@Enumerated(EnumType.STRING)
	private Genre genre;

	@Enumerated(EnumType.STRING)
	private Language language;

	@Enumerated(EnumType.STRING)
	private Rating rating;

	@Column
	@Lob
	private byte[] imageSnapshot;

	public GifEntity() {
		super();
	}

	public GifEntity(Gif gif) {
		this.setName(gif.getName());
		this.setImageSnapshot(gif.getImageSnapshot());
		this.setFilename(gif.getFilename());
		this.setImage(gif.getImage());

		this.setGenre(gif.getGenre());
		this.setLanguage(gif.getLanguage());
		this.setRating(gif.getRating());
	}

	public Gif convert(boolean isRoot) {

		Gif gif = new Gif();
		gif.setId(this.getId());
		gif.setName(this.getName());
		gif.setFilename(this.getFilename());
		gif.setGenre(this.getGenre());
		gif.setImage(this.getImage());
		gif.setImageSnapshot(this.getImageSnapshot());
		gif.setLanguage(this.getLanguage());
		gif.setRating(this.getRating());
		if (this.getCategory() != null) {
			gif.setCategory(this.getCategory().convert(false));
		}
		return gif;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public CategoryEntity getCategory() {
		return category;
	}

	public void setCategory(CategoryEntity category) {
		this.category = category;
	}

	public byte[] getImage() {
		return image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}

	public Genre getGenre() {
		return genre;
	}

	public void setGenre(Genre genre) {
		this.genre = genre;
	}

	public Language getLanguage() {
		return language;
	}

	public void setLanguage(Language language) {
		this.language = language;
	}

	public Rating getRating() {
		return rating;
	}

	public void setRating(Rating rating) {
		this.rating = rating;
	}

	public byte[] getImageSnapshot() {
		return imageSnapshot;
	}

	public void setImageSnapshot(byte[] imageSnapshot) {
		this.imageSnapshot = imageSnapshot;
	}

	public List<UserEntity> getUsers() {
		return users;
	}

	public void setUsers(List<UserEntity> users) {
		this.users = users;
	}

}
