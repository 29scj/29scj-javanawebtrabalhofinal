package br.com.fiap.netgifx.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import br.com.fiap.netgifx.models.User;

@Entity
@Table(name = "USER")
public class UserEntity implements Serializable {
	private static final long serialVersionUID = 5890518935144726359L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "NOME", nullable = false, length = 200)
	private String name;

	@Column(name = "LOGIN",unique=true)
	private String login;

	@Column(name = "SENHA", nullable = false, length = 10)
	private String password;

	@Column(name = "DATA_CADASTRO")
	private Date createdAt;

	@Column(name = "DESCRICAO")
	private String description;

	@Column(name = "ADMIN")
	private boolean admin;

	@ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.DETACH, CascadeType.REMOVE, CascadeType.PERSIST, CascadeType.REFRESH})
	@JoinTable(name = "USER_GIF", catalog = "mysqldb", joinColumns = {
			@JoinColumn(name = "userId", nullable = false, updatable = false) },
			inverseJoinColumns = { @JoinColumn(name = "gifId",
					nullable = false, updatable = false) })
	private List<GifEntity> gifsEntity = new ArrayList<>();
	
	public UserEntity() {
		super();
	}

	public UserEntity(User user) {
		this.setName(user.getName());
		this.setLogin(user.getLogin());
		this.setPassword(user.getPassword());
		this.setDescription(user.getDescription());
		this.setAdmin(user.isAdmin());
	}

	public User convert() {
		User user = new User();
		user.setId(this.id);
		user.setName(this.name);
		user.setLogin(this.login);
		user.setDescription(this.description);
		user.setAdmin(this.admin);
		return user;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isAdmin() {
		return admin;
	}

	public void setAdmin(boolean admin) {
		this.admin = admin;
	}

	public List<GifEntity> getGifsEntity() {
		return gifsEntity;
	}

	public void setGifsEntity(List<GifEntity> gifsEntity) {
		this.gifsEntity = gifsEntity;
	}

}