package br.com.fiap.netgifx.dao;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import br.com.fiap.netgifx.entities.GifEntity;
import br.com.fiap.netgifx.entities.UserEntity;
import br.com.fiap.netgifx.models.Gif;
import br.com.fiap.netgifx.models.User;

@Stateless
public class UserDAO extends AbstractDAO<UserEntity> {

	@PersistenceContext(unitName = "netgifx")
	private EntityManager em;

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}

	public UserDAO() {
		super(UserEntity.class);
	}

	public User findByLoginAndPassword(String login, String password) {

		try {
			UserEntity user = (UserEntity) em
					.createQuery("SELECT u from UserEntity u where u.login = :login and u.password = :password")
					.setParameter("login", login).setParameter("password", password).getSingleResult();
			return user.convert();
		} catch (NoResultException e) {
			return null;
		}
	}
}
