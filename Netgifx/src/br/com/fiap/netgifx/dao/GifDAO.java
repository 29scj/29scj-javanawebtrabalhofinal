package br.com.fiap.netgifx.dao;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import br.com.fiap.netgifx.entities.GifEntity;
import br.com.fiap.netgifx.models.Gif;

@Stateless
public class GifDAO extends AbstractDAO<GifEntity> {

	@PersistenceContext(unitName = "netgifx")
	private EntityManager em;

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}

	public GifDAO() {
		super(GifEntity.class);
	}

	@SuppressWarnings("unchecked")
	public List<Gif> findByUserId(Long id) {

		try {
			List<GifEntity> gifsEntity = em.createQuery("SELECT g from GifEntity g JOIN g.users u where u.id = :id")
					.setParameter("id", id).getResultList();

			List<Gif> gifs = new ArrayList<>();
			for (GifEntity gifEntity : gifsEntity) {
				gifs.add(gifEntity.convert(true));
			}
			return gifs;
		} catch (NoResultException e) {
			return null;
		}
	}
}
