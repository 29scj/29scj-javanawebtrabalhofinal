package br.com.fiap.netgifx.dao;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import br.com.fiap.netgifx.entities.CategoryEntity;

@Stateless
public class CategoryDAO extends AbstractDAO<CategoryEntity> {
	
	@PersistenceContext(unitName = "netgifx")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CategoryDAO() {
        super(CategoryEntity.class);
    }		

}
