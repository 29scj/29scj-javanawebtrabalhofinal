package br.com.fiap.netgifx.filter;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import javax.faces.application.ResourceHandler;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebFilter("/*")
public class SessionFilter implements Filter {

	private static final Set<String> ALLOWED_PATHS = Collections.unmodifiableSet(
			new HashSet<>(Arrays.asList("", "/login.xhtml",
					"/css/login.css",
					"/javax.faces.resource/core.js.xhtml",
					"/javax.faces.resource/theme.css.xhtml",
					"/javax.faces.resource/jquery/jquery.js.xhtml",
					"/javax.faces.resource/components.css.xhtml", 
					"/javax.faces.resource/jquery/jquery-plugins.js.xhtml", 
					"/javax.faces.resource/components.js.xhtml")));

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;
		// User user = (User) req.getSession(true).getAttribute("user");
		String url = req.getRequestURL().toString();
		HttpSession session = req.getSession(false);

		String path = req.getRequestURI().substring(req.getContextPath().length()).replaceAll("[/]+$", "");
		// String path = url.substring(url.lastIndexOf('/')+1, url.length());
		boolean loggedIn = (session != null && session.getAttribute("user") != null);
		boolean allowedPath = ALLOWED_PATHS.contains(path);

		if (loggedIn || allowedPath) {
			chain.doFilter(req, res);
		} else {
			res.sendRedirect(req.getContextPath() + "/login.xhtml");
		}
	}

	// if ((!path.equals("login.xhtml") && !path.equals("")) && user == null) {
	// req.getRequestDispatcher("/login.xhtml?faces-redirect=true").forward(req,
	// response);
	// }
	// chain.doFilter(request, response);
	// }

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub

	}

}